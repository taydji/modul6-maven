import java.util.Scanner;

public class BlackJack {
    public static void main(String[] args) {

	Scanner sc = new Scanner(System.in);
	Deck d = new Deck(1, true);

	Player pl = new Player("The Player");
	Player dealer = new Player("Dealer");

	// 2 cards player
	pl.addCard(d.dealNextCard());
	dealer.addCard(d.dealNextCard());
	// 2
	
	pl.addCard(d.dealNextCard());
	dealer.addCard(d.dealNextCard());

	// hands

	System.out.println("Cards are dealt\n  ");
	pl.printHand(true);
	dealer.printHand(false);
	System.out.println("\n");

	boolean plKlar = false;
	boolean dealerKlar = false;

	String ans;

	while (!plKlar || !dealerKlar) {
	    // player turn
	    if (!plKlar) {

		System.out.print(" HIT OR STAY? ( ENTER [H] to hit or[ S] to stay )");
		ans = sc.next();
		System.out.println();
		// PLAYER HITS
		if (ans.compareToIgnoreCase("H") == 0) {
		    // THE NEXT CARD TO THE DECK

		   plKlar = !pl.addCard(d.dealNextCard()); // true value
		   pl.printHand(true);

		} else {
		    plKlar = true;
		}
	    }
	    // dealers turn
	    if (!dealerKlar) {
		if (dealer.getHandSum() < 17) {
		    System.out.println(" THE DEALER HITS \n");
		  dealerKlar = !dealer.addCard(d.dealNextCard());
		    dealer.printHand(false);

		} else {
		    System.out.println(" THE DEALER HITS \n");
		    dealerKlar = true;
		}
	    }
	    System.out.println();
	}
	// close scanner
	sc.close();

	pl.printHand(true);
	dealer.printHand(true);
	int plSum = pl.getHandSum();
	int dealerSum = dealer.getHandSum();

	if (plSum > dealerSum && plSum <= 21 || dealerSum > 21) {
	    System.out.println("YOU WIN");

	} else {
	    System.out.println("THE DEALER WINS");
	}
    }
}
